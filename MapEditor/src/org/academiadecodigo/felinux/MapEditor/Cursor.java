package org.academiadecodigo.felinux.MapEditor;

import org.academiadecodigo.simplegraphics.graphics.Color;

public class Cursor extends Cell {


    public int directionX;
    public int directionY;
    private boolean painting;
    private boolean deleting;

    public Cursor(int col, int row){

        super(col, row);
        super.getRect().setColor(Color.RED);
        super.getRect().fill();
    }

    public void move(){


        int currentX = super.getCol();
        int currentY = super.getRow();

        //magic 1 for rounding problems
        //check left
        if(currentX + directionX < 0){

            if(directionX < 0){
                directionX = 0;
            }
        }
        //check right
        if(currentX + directionX > Grid.totalCols){

            if(directionX > 0){
                directionX = 0;
            }
        }
        //check up
        if(currentY + directionY < 0){

            if(directionY < 0){
                directionY = 0;
            }
        }
        //check down
        if(currentY + directionY > Grid.totalRows){

            if(directionY > 0){
                directionY = 0;
            }
        }

        getRect().translate(directionX*Grid.cellWidth,directionY*Grid.cellHeight);
        super.setCol(super.getCol()+directionX);
        super.setRow(super.getRow()+directionY);
    }

    public void paint() {
        painting = true;
    }

    public void stopPaint(){
        painting = false;
    }

    public boolean isPainting() {
        return painting;
    }

    public void delete() {
        deleting = true;
    }

    public void stopDelete() {
        deleting = false;
    }

    public boolean isDeleting(){
        return deleting;
    }
}
