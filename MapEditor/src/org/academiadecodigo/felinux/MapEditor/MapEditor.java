package org.academiadecodigo.felinux.MapEditor;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class MapEditor implements KeyboardHandler {


    private Grid grid;
    private Cursor cursor;
    private Keyboard keyboard;

    public MapEditor(int cols,int rows){

        grid = new Grid(cols, rows);
        grid.drawCells();
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        cursor.directionX = 0;
        cursor.directionY = 0;

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_W){
            cursor.directionY -= 1;
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_S){
            cursor.directionY += 1;
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_A){
            cursor.directionX -= 1;
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_D){
            cursor.directionX += 1;
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_H){
            cursor.paint();
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_J){
            cursor.delete();
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_C){
            grid.clear();
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_K){
            grid.saveState();
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_L){
            grid.loadState();
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_W){
            cursor.directionY = 0;
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_S){
            cursor.directionY = 0;
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_A){
            cursor.directionX = 0;
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_D){
            cursor.directionX = 0;
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_H){
            cursor.stopPaint();
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_J){
            cursor.stopDelete();
        }
    }

    public void setup() {

        this.cursor = new Cursor(0,0);
        this.keyboard = new Keyboard(this);
        init();
    }

    /**
     * keyboard init
     */
    private void init(){

        KeyboardEvent w = new KeyboardEvent();
        w.setKey(KeyboardEvent.KEY_W);
        w.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent s = new KeyboardEvent();
        s.setKey(KeyboardEvent.KEY_S);
        s.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent a = new KeyboardEvent();
        a.setKey(KeyboardEvent.KEY_A);
        a.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent d = new KeyboardEvent();
        d.setKey(KeyboardEvent.KEY_D);
        d.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent paint = new KeyboardEvent();
        paint.setKey(KeyboardEvent.KEY_H);
        paint.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent erase = new KeyboardEvent();
        erase.setKey(KeyboardEvent.KEY_J);
        erase.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent save = new KeyboardEvent();
        save.setKey(KeyboardEvent.KEY_K);
        save.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent load = new KeyboardEvent();
        load.setKey(KeyboardEvent.KEY_L);
        load.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent clearAll = new KeyboardEvent();
        clearAll.setKey(KeyboardEvent.KEY_C);
        clearAll.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        //key Release

        KeyboardEvent stopPaint = new KeyboardEvent();
        stopPaint.setKey(KeyboardEvent.KEY_H);
        stopPaint.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);

        KeyboardEvent wStop = new KeyboardEvent();
        wStop.setKey(KeyboardEvent.KEY_W);
        wStop.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);

        KeyboardEvent sStop = new KeyboardEvent();
        sStop.setKey(KeyboardEvent.KEY_S);
        sStop.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);

        KeyboardEvent aStop = new KeyboardEvent();
        aStop.setKey(KeyboardEvent.KEY_A);
        aStop.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);

        KeyboardEvent dStop = new KeyboardEvent();
        dStop.setKey(KeyboardEvent.KEY_D);
        dStop.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);

        KeyboardEvent stopErase = new KeyboardEvent();
        stopErase.setKey(KeyboardEvent.KEY_J);
        stopErase.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);

        //event listeners
        keyboard.addEventListener(w);
        keyboard.addEventListener(s);
        keyboard.addEventListener(a);
        keyboard.addEventListener(d);
        keyboard.addEventListener(paint);
        keyboard.addEventListener(erase);
        keyboard.addEventListener(save);
        keyboard.addEventListener(load);
        keyboard.addEventListener(clearAll);
        keyboard.addEventListener(stopPaint);
        keyboard.addEventListener(wStop);
        keyboard.addEventListener(sStop);
        keyboard.addEventListener(aStop);
        keyboard.addEventListener(dStop);
        keyboard.addEventListener(stopErase);
    }

    public void start() {

        while(true){
            try {
                Thread.sleep(60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            cursor.move();
            paintCells();
            deleteCells();
        }
    }

    private void deleteCells() {

        if(cursor.isDeleting()){
            grid.deleteCells(cursor.getCol(),cursor.getRow());
        }
    }

    private void paintCells() {

        if(cursor.isPainting()){
            grid.paintCell(cursor.getCol(),cursor.getRow());
        }
    }
}
