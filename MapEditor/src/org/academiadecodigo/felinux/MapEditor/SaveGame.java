package org.academiadecodigo.felinux.MapEditor;

import org.academiadecodigo.felinux.Main;

import java.io.*;
import java.util.ArrayList;

public class SaveGame {


    BufferedWriter bWriter;
    BufferedReader bReader;
    LineNumberReader lineReader;

    public SaveGame() {

    }

    public void save(ArrayList<Cell> gameState) {

        try {
            FileWriter fileWriter = new FileWriter(Main.FILE_PATH);
            bWriter = new BufferedWriter(fileWriter);

            for(Cell cell : gameState) {

                bWriter.write(cell.getStatus()+"\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }finally {

            try {
                bWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<String> load() {

        ArrayList<String> loadState = new ArrayList<>();

        try {
            FileReader fileReader = new FileReader(Main.FILE_PATH);
            bReader = new BufferedReader(fileReader);
            lineReader = new LineNumberReader(bReader);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            String line = lineReader.readLine();
            loadState.add(line);

            while(line!=null){

                line = null;
                line = lineReader.readLine();

                loadState.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally{

            try {
                lineReader.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return loadState;
    }
}

