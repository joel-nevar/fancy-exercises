package org.academiadecodigo.felinux.MapEditor;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.ArrayList;

public class Grid {


    public static final int PADDING = 10;
    private final int pixelWidth = 700;
    private final int pixelHeight = 700;
    private ArrayList<Cell> cellArray;
    public static int totalCols;
    public static int totalRows;
    public static float cellWidth;
    public static float cellHeight;
    private SaveGame saveGame;

    public Grid(int cols, int rows){

        Rectangle grid = new Rectangle(PADDING,PADDING,pixelWidth,pixelHeight);
        grid.setColor(Color.WHITE);
        grid.draw();
        totalCols = cols;
        totalRows = rows;
        cellArray = new ArrayList<>();
        cellWidth = pixelWidth/totalCols;
        cellHeight = pixelHeight/totalRows;
        this.saveGame = new SaveGame();
    }

    public void drawCells() {

        for (int i = 0; i<= totalCols;i++){

            for (int j = 0; j <= totalRows; j++){

                cellArray.add(new Cell(i,j));
            }
        }
    }

    public void paintCell(int col, int row) {

        for (Cell cell : cellArray) {

            if(cell.getCol()==col&&cell.getRow()==row){
                cell.fill();
            }
        }
    }

    public void deleteCells(int col, int row) {

        for (Cell cell : cellArray) {

            if(cell.getCol()==col&&cell.getRow()==row){
                cell.hide();
            }
        }
    }

    public void saveState(){

        saveGame.save(cellArray);
        System.out.println("game saved");
    }

    public void clear() {

        for (Cell cell : cellArray) {

            cell.hide();
        }
    }

    public void loadState() {

        ArrayList<String> gameState = saveGame.load();

        for (int i = 0; i < cellArray.size(); i++) {

            if (gameState.get(i).equals("true")) {
                cellArray.get(i).fill();
                continue;
            }
            cellArray.get(i).hide();
        }
    }
}
