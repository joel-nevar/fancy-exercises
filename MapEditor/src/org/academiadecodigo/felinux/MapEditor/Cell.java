package org.academiadecodigo.felinux.MapEditor;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cell {


    private int col;
    private int row;
    private boolean status;
    private Rectangle rect;

    public Cell(int col, int row){

        this.col = col;
        this.row = row;

        this.rect = new Rectangle(Grid.PADDING+col*Grid.cellWidth,Grid.PADDING+row*Grid.cellHeight,Grid.cellWidth,Grid.cellHeight);
        rect.setColor(Color.BLACK);
        rect.draw();
    }

    public Rectangle getRect() {

        return rect;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public void fill() {

        status = true;
        rect.setColor(Color.BLUE);
        rect.fill();
    }

    public void hide() {

        status = false;
        rect.setColor(Color.BLACK);
        rect.draw();
    }

    public boolean getStatus() {
        return status;
    }
}
