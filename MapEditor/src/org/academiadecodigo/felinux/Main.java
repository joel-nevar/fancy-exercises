package org.academiadecodigo.felinux;

import org.academiadecodigo.felinux.MapEditor.MapEditor;

public class Main {


    public final static String FILE_PATH = "resources/saveState.txt";

    public static void main(String[] args) {

        MapEditor game = new MapEditor(20,20);
        game.setup();
        game.start();
    }
}
