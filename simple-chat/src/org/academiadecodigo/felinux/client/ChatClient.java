package org.academiadecodigo.felinux.client;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.System.err;

public class ChatClient {


    private Socket client;
    private ExecutorService singleThread;

    public static void main(String[] args) {

        try{

            String hostname = args[0];
            int portNumber = Integer.parseInt(args[1]);
            ChatClient client = new ChatClient( new Socket(hostname, portNumber) );
            client.transmit();

        }catch (IndexOutOfBoundsException e){
            err.println("Insert chat ip and port at program execution");

        } catch (IOException e) {
            err.println("Failed to connect to server");
        }
    }

    public ChatClient(Socket client){

        this.client = client;
        this.singleThread = Executors.newSingleThreadExecutor();
        singleThread.submit(new ServerListener(client,this));
    }

    private void transmit() throws IOException {

        System.out.println("Connected!\nType /help for chat commands.");
        PrintWriter writer = new PrintWriter(client.getOutputStream(),true);
        Scanner input = new Scanner(System.in);

        while(client.isBound()){

            writer.println(input.nextLine());
        }
    }

    public void write(String message){

        System.out.println(message);
    }
}
