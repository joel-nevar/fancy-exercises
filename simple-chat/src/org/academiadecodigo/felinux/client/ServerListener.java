package org.academiadecodigo.felinux.client;

import java.io.*;
import java.net.Socket;

import static java.lang.System.err;

public class ServerListener implements Runnable{


    private ChatClient client;
    private Socket serverOut;
    private BufferedReader reader;

    public ServerListener(Socket server, ChatClient client){

        this.client = client;
        this.serverOut = server;
    }

    @Override
    public void run() {

        try {

            reader = new BufferedReader(new InputStreamReader(serverOut.getInputStream()));
            interpretServer();

        } catch (IOException  e) {
            err.println("Connection to server lost");
        }
    }

    private void interpretServer() throws IOException {

        while (serverOut.isBound()){

            String message;

            if((message = reader.readLine())!=null){

                client.write(message);
            }
        }
    }
}
