package org.academiadecodigo.felinux.server;

import org.academiadecodigo.felinux.server.commands.*;

public enum CommandType {


    MESSAGE("", "", new MessageHandler()),

    HELP("Displays a list with the chat commands", "", new HelpHandler()),

    NAME("Changes chat username","/name <new username>", new NameHandler()),

    QUIT("Closes the chat client","/quit", new QuitHandler()),

    MUTE("Mute or unmute username","/mute <username>", new MuteHandler()),

    LIST("Displays list of current chat users", "/list", new ListHandler()),

    WHISPER("Send a direct message to a client by name", "/whisper <username> <message>",
            new WhisperHandler()),

    INVALID("","", new InvalidHandler());

    private final String description;
    private final String usage;
    private final CommandHandler command;

    CommandType(String description, String usage, CommandHandler command){

        this.command = command;
        this.usage = usage;
        this.description = description;
    }

    public String getDescription() {

        return description;
    }

    public String getUsage() {

        return usage;
    }

    public CommandHandler getCommand() {
        return command;
    }

    public static CommandHandler getCommandHandler(String message){

        if(message.startsWith("/")){

            String command = message.substring(1).split("\\s+")[0];

            for (CommandType commandType: values()) {

                if (command.equals(commandType.name().toLowerCase())){

                    return commandType.getCommand();
                }
            }

            return INVALID.getCommand();
        }

        return MESSAGE.getCommand();
    }
}
