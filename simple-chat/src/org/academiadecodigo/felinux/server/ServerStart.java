package org.academiadecodigo.felinux.server;

import static java.lang.System.err;

public class ServerStart {


    public static void main(String[] args) {

        try{

            int portNumber = Integer.parseInt(args[0]);
            Server server = new Server(portNumber);

        }catch (IndexOutOfBoundsException e){
            err.println("Input server port on startup agrs");
        }
    }
}
