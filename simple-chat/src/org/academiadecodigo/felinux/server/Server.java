package org.academiadecodigo.felinux.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {


    private static final int MAX_CLIENTS = 20;
    private ServerSocket server;
    private ExecutorService threadPool;
    private final LinkedList <ClientHandler> clients;

    public Server (int portNumber){

        threadPool = Executors.newCachedThreadPool();
        clients = new LinkedList<>();

        try {

            System.out.println("Server online on port: " + portNumber);
            server = new ServerSocket(portNumber);
            start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() throws IOException {

        int clientCounter = 0;

        while(true) {

            ClientHandler newClient =  new ClientHandler(server.accept(),this,
                    "Client " + (clientCounter++));
            System.out.println("Client Found");
            threadPool.submit(newClient);
        }
    }

    public boolean addClient(ClientHandler client) {

        synchronized (clients) {

            if(clients.size()>MAX_CLIENTS){
                return false;
            }

            return clients.add(client);
        }
    }

    public void removeClient(ClientHandler client){

        synchronized (clients) {

            clients.remove(client);
        }
    }

    public void broadcast(String message){

        for (ClientHandler client: clients ) {

            client.send(message);
        }
    }

    public ClientHandler clientByName(String name){

        synchronized (clients){

            for (ClientHandler client : clients ) {

                if(client.getClientName().equals(name)){

                    return client;
                }
            }

            return null;
        }
    }
}
