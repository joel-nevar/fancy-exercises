package org.academiadecodigo.felinux.server;

import java.io.*;
import java.net.Socket;

import static java.lang.System.err;

public class ClientHandler implements Runnable{


    private PrintWriter dataOutput;
    private LineNumberReader dataInput;
    private Server server;
    private String clientName;
    private boolean interrupt;
    private Socket clientSocket;

    public ClientHandler(Socket clientSocket, Server server, String clientName){

        try {

            this.clientName = clientName;
            this.clientSocket = clientSocket;
            this.server = server;
            this.dataInput = new LineNumberReader ( new BufferedReader ( new InputStreamReader(  clientSocket.getInputStream())));
            this.dataOutput = new PrintWriter( new BufferedWriter( new OutputStreamWriter( clientSocket.getOutputStream())),true);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

        if(!server.addClient(this)){

            send("Server full, try again later");
            System.exit(0);
        }

        while (!interrupt) {

            try {

                clientListener();

            } catch (IOException e) {
                err.println("Connection to Client: \"" + clientName + "\" Lost");
                terminateConnection();
            }
        }
    }

    public void clientListener() throws IOException{

        String message;

        if((message = dataInput.readLine())!=null){

            CommandType.getCommandHandler(message).handle(server,this,message);
        }
    }

    public void send(String message){

        dataOutput.println(message);
    }

    public void terminateConnection(){

        try {

            interrupt = true;
            server.broadcast(clientName + " has left the chat!");
            server.removeClient(this);
            dataOutput.close();
            dataInput.close();
            clientSocket.close();

        } catch (IOException r) {
            //do nothing
        }
    }

    public String getClientName() {

        return clientName;
    }

    public void setClientName(String clientName) {

        this.clientName = clientName;
    }
}
