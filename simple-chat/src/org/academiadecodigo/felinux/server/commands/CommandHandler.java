package org.academiadecodigo.felinux.server.commands;

import org.academiadecodigo.felinux.server.ClientHandler;
import org.academiadecodigo.felinux.server.Server;

public interface CommandHandler {

    /**
     * Strategy Design Pattern
     * @param server
     * @param client
     * @param message
     */
    void handle(Server server, ClientHandler client, String message);
}
