package org.academiadecodigo.felinux.server.commands;

import org.academiadecodigo.felinux.server.ClientHandler;
import org.academiadecodigo.felinux.server.CommandType;
import org.academiadecodigo.felinux.server.Server;

import static org.academiadecodigo.felinux.server.CommandType.*;

public class HelpHandler implements CommandHandler {


    @Override
    public void handle(Server server, ClientHandler client, String message) {

        StringBuilder builder = new StringBuilder();

        for (CommandType command : CommandType.values()) {

            if(command==INVALID||command==MESSAGE){
                continue;
            }

            builder.append("/").append(command.name().toLowerCase()).append(" ")
                    .append(command.getDescription()).append("\n");
        }

        client.send(builder.toString());
    }
}
