package org.academiadecodigo.felinux.server.commands;

import org.academiadecodigo.felinux.server.ClientHandler;
import org.academiadecodigo.felinux.server.Server;

public class NameHandler implements CommandHandler {


    @Override
    public void handle(Server server, ClientHandler client, String message) {

        String name = message.substring(6);

        if(!isLong(name)){

            new InvalidHandler().handle(server,client,message);
            return;
        }

        if(!isEmpty(name)){

            new InvalidHandler().handle(server,client,message);
            return;
        }

        String prevName = client.getClientName();
        client.setClientName(name);
        server.broadcast(prevName +" has changed their name to " + name);
    }

    private boolean isLong(String message) {

        return 16>message.length();
    }

    private boolean isEmpty(String message){

        return !message.trim().isEmpty();
    }
}
