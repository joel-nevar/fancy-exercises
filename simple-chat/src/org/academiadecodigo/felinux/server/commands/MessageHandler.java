package org.academiadecodigo.felinux.server.commands;

import org.academiadecodigo.felinux.server.ClientHandler;
import org.academiadecodigo.felinux.server.Server;

public class MessageHandler implements CommandHandler {


    @Override
    public void handle(Server server, ClientHandler client, String message) {

        if(isValid(message)){

            server.broadcast(client.getClientName() + " says: " + message);
        }
    }

    private boolean isValid(String message) {

        return !message.trim().isEmpty();
    }
}
