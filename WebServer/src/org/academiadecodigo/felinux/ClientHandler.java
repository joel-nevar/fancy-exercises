package org.academiadecodigo.felinux;

import java.io.*;
import java.net.Socket;
import java.util.Arrays;

public class ClientHandler implements Runnable{


    private Socket clientSocket;
    private LineNumberReader reader;
    private DataOutputStream writer;
    private DataInputStream documentReader;
    private final String path = "rsc";
    private String header;
    private File file;

    public ClientHandler(Socket clientSocket) {

        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {

        try {

            connect();
            System.out.println("client handled with care");
            disconnect();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void disconnect() throws IOException {

        reader.close();
        writer.close();
        documentReader.close();
        clientSocket.close();
    }

    private void connect() throws IOException {

        reader = new LineNumberReader( new BufferedReader(
                new InputStreamReader( clientSocket.getInputStream() )));
        writer = new DataOutputStream(clientSocket.getOutputStream());

        String request = reader.readLine();
        System.out.println(request);

        if(request == null){
            //ninjutsu
            request = "GET / something";
        }

        String[] link = request.split("\\s+");
        file = new File(path + link[1]);

        if(file.isDirectory()){

            file = new File(path + link[1] + "index.html");
        }

        fileExtension();

        documentReader = new DataInputStream(new FileInputStream(file.getPath()));
        byte[] buffer = new byte[2048];
        writer.write(header.getBytes());

        while(documentReader.read(buffer)!=-1){

            writer.write(buffer);
            Arrays.fill(buffer,(byte)0);
        }
    }

    private void fileExtension() {

        if (file.exists()) {

            if (file.getName().contains(".html")) {

                header = "HTTP/1.1 200 Document Follows\r\n" +
                        " Content-Type: text/html; charset=UTF-8\r\n" +
                        " Content-Length: " + file.length() + "\r\n\r\n";
            }

            if (file.getName().contains(".js")) {

                header = "HTTP/1.1 200 Document Follows\r\n" +
                        " Content-Type: text/javascript; charset=UTF-8\r\n" +
                        " Content-Length: " + file.length() + "\r\n\r\n";
            }

            if (file.getName().contains(".png")) {

                header = "HTTP/1.1 200 Document Follows\r\n" +
                        " Content-Type: image/.png; charset=UTF-8\r\n" +
                        " Content-Length: " + file.length() + "\r\n\r\n";
            }

            if (file.getName().contains(".ico")) {

                header = "HTTP/1.1 200 Document Follows\r\n" +
                        " Content-Type: image/.ico; charset=UTF-8\r\n" +
                        " Content-Length: " + file.length() + "\r\n\r\n";
            }
        }

        if (!file.exists()) {

            file = new File(path + "\\pagenotfound.png");
            header = "HTTP/1.1 404 Not Found\r\n" +
                    " Content-Type: text/html; charset=UTF-8\r\n" +
                    " Content-Length: " + file.length() + "\r\n\r\n";
        }
    }
}
