package org.academiadecodigo.felinux;

import java.io.*;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {


    private final int PORT = 14000;
    private ServerSocket serverSocket;
    private final ExecutorService thread = Executors.newFixedThreadPool(10);

    public static void main(String[] args) {

        Server server = new Server();
        server.init();
    }

    private void init() {

        try {

            serverStart();

            while(true){

                handleClient();

                System.out.println("request complete");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleClient() throws IOException {

        System.out.println("waiting for a request");
        thread.execute(new ClientHandler(serverSocket.accept()));

        System.out.println("Client Received");
    }


    private void serverStart() throws IOException{

        if(serverSocket == null){

            serverSocket = new ServerSocket(PORT);
            System.out.println("server online");
        }
    }
}
