"use strict";

/// EVENTS
window.onload = onLoadEvent;

/// Global variables
let cvs;
let ctx;
let d;
let lastD;

/// FUNCTIONS
function onLoadEvent()
{
    cvs = document.getElementById('snake');
    ctx = cvs.getContext('2d');
    // foodImg : new Image()
}

// create the unit
const box = 32;

//load images
const ground = new Image();
ground.src = "img/ground.png";

const foodImg = new Image();
foodImg.src = "img/food.png";0

//load sound
const dead = new Audio();
const down = new Audio();
const eat = new Audio();
const left = new Audio();
const right = new Audio();
const up = new Audio();

// dead.src = "audio\dead.mp3";
// down.src = "audio\down.mp3";
// eat.src = "audio\eat.mp3";
// left.src = "audio\left.mp3";
// right.src = "audio\right.mp3";
//up.src = "audio\up.mp3";

//create the snake
let snake = [];
 snake[0] = {
     x : 9 * box,
     y : 10 * box
 }

//create the food
let food = {
    x : Math.floor(Math.random()*17+1)*box,
    y : Math.floor(Math.random()*15+3)*box
}

//create the score var

let score = 0;

// control snake

const keyEvents = {
    left : 37,
    up : 38,
    right : 39,
    down : 40
}


// document.addEventListener("keydown",keydownEvent);
window.onkeydown = keydownEvent;
function keydownEvent(ev){
    if ((ev.keyCode === keyEvents.up)&& d != "DOWN"){d = "UP"}
    else if ((ev.keyCode === keyEvents.down)&& d !="UP"){d = "DOWN"}
    else if ((ev.keyCode === keyEvents.right)&& d !="LEFT"){d = "RIGHT"}
    else if ((ev.keyCode === keyEvents.left)&& d != "RIGHT"){d = "LEFT"}
};

function collision(head, array){
    for(let i = 0; i < array.length; i++){
        if(head.x == array[i].x && head.y == array[i].y){
            return true;
        }
    }
    return false;
};

// draw everthing to the canvas

var draw = function(){

    ctx.drawImage(ground,0,0);

    for (let i = 0; i < snake.length ; i++){
        ctx.fillStyle = (i == 0)? "green" : "white";
        
    // for (let i = 0; i < snake.lenght ; i++){
    //     ctx.fillStyle = (i == 0)? "green" : "white";
    //     if(i==0){
    //         ctx.fillStyle = "green";
    //     } 
    //     else{ ctx.fillStyle = "white"
    // };

        ctx.fillRect(snake[i].x,snake[i].y,box,box);

        ctx.strokeStyle = "red";
        ctx.strokeRect(snake[i].x,snake[i].y,box,box);
    }

    ctx.drawImage(foodImg, food.x, food.y)

    //old head position
    let snakeX = snake[0].x;
    let snakeY = snake[0].y;




    //which direction
    switch(d){
        case "LEFT":
            snakeX -= box;
            //left.play();
            break;
        case "UP":
            snakeY -= box;
            //up.play();
            break;
        case "RIGHT":
            snakeX += box;
            //right.play();
            break;
        case "DOWN":
            snakeY += box
            //down.play();
            break;   
        }

     //add new head
     let newHead = {
        x:snakeX,
        y:snakeY
    }

    //if eats the food

    if(snakeX == food.x && snakeY == food.y){
        score++
        //eat.play();
        food = {
            x : Math.floor(Math.random()*17+1)*box,
            y : Math.floor(Math.random()*15+3)*box
        }
    }else{
        //remove tail
        snake.pop();
    }
    //Game Over
    if(snakeX < box || snakeX > 17*box || snakeY <3*box 
    || snakeY > 17*box || collision(newHead,snake)){
        clearInterval(game);  
        //dead.play();     
    }



    snake.unshift(newHead)
        ctx.fillStyle = "white";
        ctx.font = "45px Changa one";
        ctx.fillText(score, 2*box,1.6*box);
}

//call draw every 100ms

let game = setInterval(draw,100);
